<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 */
?>
	
		</div><!-- end #main -->
				
		<footer>
			<div class="container aligncenter">
				<div class="footer_nav clearfix">
					<ul class="navigation">
						<?php wp_list_pages('title_li=&include=11,13,15'); ?>
					</ul>
					<div class="footer_logo"><img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.svg" alt="<?php wp_title('|',true,'right'); ?>" /></div>
					<ul class="navigation">
						<?php wp_list_pages('title_li=&include=17,19,21'); ?>
					</ul>
				</div>
				<div class="copyright">
				<p>PO Box 156, Harbor Springs, MI 49740 Tel: 616.988.7788</p>
				<p>&copy; <?php echo date('Y'); ?> Integrated Barcoding a division of SNF Co., Inc  /  Site Crafted By <a href="http://edencreative.co" target="_blank">Eden</a></p>
				</p>
			</div>
		</footer>
	
	</div><!-- #page_container -->

	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.singlePageNav.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.backstretch.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.stellar.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
	<!-- <script type="text/javascript" src="js/scripts.min.js"></script> -->
		
</body>
</html>
