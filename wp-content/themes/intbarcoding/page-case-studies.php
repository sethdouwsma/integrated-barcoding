<?php 
/* 
Template Name: Case Studies
*/
?>

<?php get_header(); ?>

	<div class="section content container">
		<h2 class="aligncenter"><?php the_title(); ?></h2>				
		<ul class="case_study_list">
			<?php 
			    query_posts(array( 
			        'post_type' => 'case_study',
			        'showposts' => -1 
			    ) );
			?>
			<?php while (have_posts()) : the_post(); ?>
				<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
				<li style="background-image: url(<?php echo $feat_image; ?>);">
					<a href="<?php the_permalink() ?>"></a>
					<div class="overlay"></div>
					<div class="container">
				        <h4><?php the_title(); ?></h4>
				        <a href="<?php the_permalink() ?>" class="button green">Read More</a>
					</div>
				</li>
			<?php endwhile;?>
		</ul>
	</div><!-- end .content -->


<?php get_footer(); ?>