<?php 
/* 
Template Name: Hardware Page
*/
?>

<?php get_header(); ?>


	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
			
				<div class="section intro aligncenter partners">
					<h2>Data Collection Hardware</h2>
					<p>Finding the right hardware solution to meet your needs is important in solving your specific data capture issue. This is why we partner with many hardware vendors as each vendor offers unique hardware advantage to solve a specific need.</p>
					<img src="<?php echo get_template_directory_uri();?>/images/partner_logos.png" alt="Integrated Barcoding Hardware Partners">
				</div>
				
				<div class="section gray">
					<div class="row container">
						<div class="left">
							<h2>Flexible Hardware</h2>
							<p>Because each situation is unique, we work with the leaders in data capture hardware solution to provide our customers with options. From freezer rated mobile devices, to portable printers in a retail store. We will provide you with the right choice to work with our QuikTrac software.</p>
						</div>
						<div class="right">
							<img src="<?php echo get_template_directory_uri();?>/images/hardware_plus_software.svg" alt="Barcoding Hardware Plus Software">
						</div>
					</div>
				</div>
				
				<div class="section">
					<div class="row container">
						<div class="left">
							<img src="<?php echo get_template_directory_uri();?>/images/validated_solutions.png" alt="Validated Motorola Solutions">
						</div>
						<div class="right">
							<h2>Our Solutions are Validated</h2>
							<p>Integrated Barcoding QuikTrac software has been put thru Motorola Validation testing and has meet their stringent guidelines as a software solution that integrated with multiple hardware platforms for performance and reliability.</p>
						</div>
					</div>
				</div>
				
				<div class="questions aligncenter">
					<h2>Have More Questions?</h2>
					<p>Get in touch with us, we'd love to hear from you.</p>
					<p class="questions_info"><span><a href="mailto:info@intbarcoding.com">info@intbarcoding.com</a></span><br> or<br> <span>616-988-7788</span></p>
				</div>
				<div class="trial gray">
					<div class="container aligncenter">
						<p>Start your free trial today!</p>
						<a href="<?php echo get_permalink('23'); ?>" class="button">Request A Demo</a>
					</div>
				</div>
			</div>
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
