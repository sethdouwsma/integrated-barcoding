<?php 
/* 
Template Name: Software Page
*/
?>

<?php get_header(); ?>


	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
			
				<div class="section intro aligncenter">
					<h2>QuikTrac Features & Benefits</h2>
					<p>Being able to reach your data and use it to your advantage eliminates mistakes and can put your company ahead of the competition. QuikTrac allows you to use your applications to their fullest potential by extending your integration out to the point of entry.</p>
				</div>
				
				<div class="quiktrac_window container">
					<img src="<?php echo get_template_directory_uri();?>/images/quiktrac_window.png" alt="Quiktrac">
				</div>
				
				<div class="quiktrac_offers">
					<div class="container">
						<p>QuikTrac offers three different products to help extend your applications:</p>
					</div>
				</div>
				
				<div class="section gray">
					<div class="container">
						<div class="software_header">
							<img src="<?php echo get_template_directory_uri();?>/images/quiktrac_iseries.svg" alt="QuikTrac iSeries">
							<p>QuikTrac® iSeries gives you the ability to integrate your existing iSeries application within days by simply extending your current functionality to where the data collection needs to occur. Using portable data collection or mobile devices in a LAN or WAN environment is a simple matter of “click, drag, drop” and you are ready to interact with your iSeries application.</p>
						</div>
						<div class="software_features">
							<h2>QuikTrac iSeries Features</h2>
							<ul class="col-4">
								<li>
									<h4>Controlled Development</h4>
									<p>With QuikTrac iSeries software solution, you can control all of your own development and implementation.</p>
								</li>
								<li>
									<h4>No Coding</h4>
									<p>QuikTrac iSeries is a screen integration tool that sits on top of your iSeries, therefore it does not touch any of your backend code.</p>
								</li>
								<li>
									<h4>Easy Drag and Drop</h4>
									<p>Simply Click, Drag, and Drop your host screens onto your mobile template.</p>
								</li>
								<li>
									<h4>Simple</h4>
									<p>No software loaded on your mobile devices for the iSeries.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				
				<div class="section">
					<div class="container">
						<div class="software_header">
							<img src="<?php echo get_template_directory_uri();?>/images/quiktrac_ai.svg" alt="QuikTrac Ai">
							<p>QuikTrac® gives you the ability to create your own screens for display on any TCP/IP addressable device. By using simple SQL statements, or stored procedures, you can connect to any ODBC compliant database. Creating the screen input fields, display areas, and connecting them to the proper tables and fields in your specific database is fast and simple.</p>
						</div>
						<div class="software_features">
							<h2>QuikTrac AI Features</h2>
							<ul class="col-4">
								<li>
									<h4>Fast Integration</h4>
									<p>QuikTrac-AI is the screen development tool for integration into PC server applications.</p>
								</li>
								<li>
									<h4>Easy Field Manipulation</h4>
									<p>Simple to add fields onto your mobile device.</p>
								</li>
								<li>
									<h4>Proven</h4>
									<p>Use of SQL, or stored procedures to insert, select, update, and post information into your application.</p>
								</li>
								<li>
									<h4>Controlled Growth</h4>
									<p>You control your own development and can grow your project without programming experience.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="section gray">
					<div class="container">
						<div class="software_header">
							<img src="<?php echo get_template_directory_uri();?>/images/quiktrac_mobile.svg" alt="QuikTrac Mobile">
							<p>QuikTrac® iSeries gives you the ability to integrate your existing iSeries application within days by simply extending your current functionality to where the data collection needs to occur. Using portable data collection or mobile devices in a LAN or WAN environment is a simple matter of “click, drag, drop” and you are ready to interact with your iSeries application.</p>
						</div>
						<div class="software_features">
							<h2>QuikTrac Mobile Features</h2>
							<ul class="col-4">
								<li>
									<h4>Limitless Options</h4>
									<p>With QuikTrac iSeries software solution, you can control all of your own development and implementation.</p>
								</li>
								<li>
									<h4>Screen Integration Tool</h4>
									<p>QuikTrac iSeries is a screen integration tool that sits on top of your iSeries, therefore it does not touch any of your backend code.</p>
								</li>
								<li>
									<h4>Easy Drag and Drop</h4>
									<p>Simply Click, Drag, and Drop your host screens onto your mobile template.</p>
								</li>
								<li>
									<h4>Reliable</h4>
									<p>No software loaded on your mobile devices for the iSeries.</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="questions_software aligncenter">
					<h2>Have More Questions?</h2>
					<p>Get in touch with us, we'd love to hear from you.</p>
					<p class="questions_info"><span><a href="mailto:info@intbarcoding.com">info@intbarcoding.com</a></span><br> or<br> <span>616-988-7788</span></p>
				</div>
				<div class="trial gray">
					<div class="container aligncenter">
						<p>Start your free trial today!</p>
						<a href="<?php echo get_permalink('23'); ?>" class="button">Request A Demo</a>
					</div>
				</div>
				
			</div><!-- end .content -->
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
