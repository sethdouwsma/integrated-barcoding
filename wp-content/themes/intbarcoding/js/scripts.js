jQuery(document).ready(function ($) {

	if(!Modernizr.svg) {
	    $('img[src*="svg"]').attr('src', function() {
	        return $(this).attr('src').replace('.svg', '.png');
	    });
	}
	
	if (Modernizr.touch) {
		$('.case_study_list .button').css("bottom", 0);
	}

	// Header toggle
	$('.header_toggle').click(function() {
		$('nav').toggleClass( "toggled" );
	});
	
	$( '#header_button' ).click(function(e) {
		e.preventDefault();
		$('html, body').animate({
		    scrollTop: ($('#main').first().offset().top)
		},400);
	});
    
    $('.wpcf7-form input').focus(function(){
		$(this).closest(".form_field").toggleClass("focused");
		
	}).blur(function(){
		$(this).closest(".form_field").toggleClass("focused");
	});
	
	$('.wpcf7-form textarea').focus(function(){
		$(this).closest(".form_field").toggleClass("focused");
		
	}).blur(function(){
		$(this).closest(".form_field").toggleClass("focused");
	});
	
});
