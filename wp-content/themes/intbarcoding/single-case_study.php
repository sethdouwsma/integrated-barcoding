<?php 
/* 
Displays Case Studies Single Content
*/
?>

<?php get_header(); ?>

		<div class="section content">
			<article class="container">
				<div class="cs_header">
					<h2><?php the_title(); ?></h2>
					<div class="cs_meta">
						<p><?php the_field('location'); ?></p>
						<?php if( have_rows('application') ): ?>
							<ul class="application">
							<?php  while ( have_rows('application') ) : the_row(); ?>
								<li><?php the_sub_field('application'); ?></li>
							<?php endwhile; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
				<?php while (have_posts()) : the_post(); ?>
					<div class="entry_content"><?php the_content(); ?></div>
				<?php endwhile; ?>	
					
				<div class="cs_extras">
					<?php if( have_rows('hardware')): ?>
						<ul class="extra_bucket">
						<h3>Hardware Used</h3>
						<?php while(have_rows('hardware')): the_row(); ?>
							<li><?php the_sub_field('item'); ?></li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
					
					<?php if(have_rows('benefits')) : ?>
						<ul class="extra_bucket">
						<h3>Benefits</h3>
						<?php while(have_rows('benefits')): the_row(); ?>
							<li><?php the_sub_field('benefit'); ?></li>
						<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>	
			</article>
			
			<?php if( get_field('testimonial') ): ?>
				<div class="testimonial section gray aligncenter">
					<div class="container">
						<h2>Some Positive Feedback</h2>
						<p><?php the_field('testimonial'); ?></p>
						<p>— <?php the_field('testimonial_name'); ?></p>
					</div>
				</div>
			<?php endif; ?>
			
			<div class="container aligncenter">
				<h2>Other Case Studies</h2>
				<ul class="case_study_list">
					<?php 
					    query_posts(array( 
					        'post_type' => 'case_study',
							'orderby' => 'rand',
					        'showposts' => 3 
					    ) );
					?>
					<?php while (have_posts()) : the_post(); ?>
						<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
						<li style="background-image: url(<?php echo $feat_image; ?>);">
							<a href="<?php the_permalink() ?>"></a>
							<div class="overlay"></div>
							<div class="container">
						        <h4><?php the_title(); ?></h4>
						        <a href="<?php the_permalink() ?>" class="button green">Read More</a>
							</div>
						</li>
					<?php endwhile;?>
				</ul>
			</div>
		</div><!-- end .content -->
				

<?php get_footer(); ?>