<?php 
/* 
Template Name: Contact/Request Demo Pages
*/
?>

<?php get_header(); ?>

	<div class="section container">				
		<div class="inner">
			<h2><?php the_title(); ?></h2>
			<?php while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile;?>
		</div><!-- end .inner -->
		
		<?php get_sidebar('contact'); ?>
		
	</div><!-- end .container -->

<?php get_footer(); ?>