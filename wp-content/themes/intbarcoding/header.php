<!DOCTYPE HTML>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>

	<meta charset="utf-8">
	
	<!-- Google Chrome Frame for IE -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php wp_title('|',true,'right'); ?></title>
	
	<!-- mobile meta (hooray!) -->
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no"/>
	
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="/favicon.ico">
	<![endif]-->
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css" type="text/css" media="screen">
	
	<script type="text/javascript" src="//use.typekit.net/hqi4ise.js"></script>
	<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	
	<!--[if IE]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie.css" type="text/css" media="screen"><![endif]-->
	<!--[if IE]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<!-- drop Google Analytics Here -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-19082196-39', 'auto');
	  ga('send', 'pageview');
	
	</script>
	<!-- end analytics -->

</head>

<body <?php body_class(); ?>>
	
	<nav class="nav_container">
		<div class="container clearfix">
			<div class="logo">
				<a href="/"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg" alt="Integrated Barcoding" /></a>
			</div>
			<p class="header_toggle">&#9776;</p>
			<ul class="navigation">
				<?php wp_list_pages('title_li=&exclude=4'); ?>
			</ul>
		</div>
	</nav>
	
	<div id="page_container">
	
		<header>
			<div class="container">
				<?php if ( is_front_page() ) : ?>
					<div class="header_icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/home_icon.svg" alt="Barcoding Hardware & Software">
					</div>
					
				<?php elseif ( is_page('Hardware') ) : ?>
					<div class="header_icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/hardware_icon.svg" alt="Barcoding Hardware">
					</div>
					
				<?php elseif ( is_page('Software') ) : ?>
					<div class="header_icon">
						<img src="<?php echo get_template_directory_uri(); ?>/images/software_icon.svg" alt="Barcoding Software">
					</div>
					
				<?php endif ; ?>
				
					
				<?php if ( is_page('17') || is_singular( 'case_study' ) ) : ?>
					<h1><?php the_field('page_header', 17); ?></h1>
					<p><?php the_field('page_subheader', 17); ?></p>
					
				<?php elseif ( is_home() || is_single() || is_archive() ) : ?>
					<h1><?php the_field('page_header', 21); ?></h1>
					<p><?php the_field('page_subheader', 21); ?></p>
					
				<?php else : ?>
					<h1><?php the_field('page_header'); ?></h1>
					<p><?php the_field('page_subheader'); ?></p>
					
				<?php endif ; ?>
			
				<?php if ( is_front_page() ) : ?>
					<a href="<?php echo get_permalink('23'); ?>" class="header_button">Request A Demo</a>
					
				<?php elseif ( is_home() || is_single() || is_archive() ) : ?>
					<a href="#" id="header_button" class="header_button">Read More</a>
					
				<?php else : ?>
					<a href="#" id="header_button" class="header_button">Learn More</a>
					
				<?php endif ; ?>
			</div>
			
		</header><!-- end header -->
		
		<div id="main">
