<?php 
/* 
Template Name: About Page
*/
?>

<?php get_header(); ?>


	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
			
				<div class="section alignleft container">
					<div class="company_description">
						<h2>About Us</h2>
						<p>Whatever your business, Integrated Barcoding can provide a data collection systems to meet your needs and improve your business performance. In business since 1988, Integrated Barcoding has grown from a hardware provider to a full-service data collection solutions provider. Integrated Barcoding specializes in bringing together hardware and software solutions and customizing them to meet the individual needs of each of our customers.</p>
					</div>
					<div class="about_image">
						<img src="<?php echo get_template_directory_uri(); ?>/images/about_image.png" alt="About Integrated Barcoding">
					</div>
				</div>
				
				<div class="section gray">
					<div class="container why_were_different">
						<div class="intro aligncenter">
							<h2>Why We’re Different</h2>
							<p>With our unique approach, IB will become a member of your implementation committee to ensure a successful completion of your project. IB can provide:</p>
						</div>
						<ul>
							<li>
								<h4>Consulting</h4>
								<p>With over 50 years of experience on our staff, we understand your issues and we can provide solutions to help you resolve them.</p>
							</li>
							<li>
								<h4>System Design</h4>
								<p>Our team can provide you with a step by step system design flow from receiving thru shipping in a detailed document for your review.</p>
							</li>
							<li>
								<h4>Software</h4>
								<p>We developed our own software solution tools over the years to meet our customer’s needs and give our clients the ability to make their own changes as needed.</p>
							</li>
							<li>
								<h4>Hardware</h4>
								<p>We work with many hardware solutions so that we can recommend the right solution to resolve your hardware needs.</p>
							</li>
							<li>
								<h4>Integration</h4>
								<p>Being able to integrate the hardware and software as one on a project eliminates a potential weak link.</p>
							</li>
							<li>
								<h4>Implementation</h4>
								<p>Our team bring years of experience and knowledge on the hardware, software, wireless back bone, and even media solutions for your printers that are crucial to your success.</p>
							</li>
							<li>
								<h4>Training</h4>
								<p>Our approach is to make sure your team has the ability to support yourself from making changes to projects, to creating new ones and being able to support your hardware solutions too.</p>
							</li>
							<li>
								<h4>Ongoing Support</h4>
								<p>We are available to help answer all your questions when needed from software development, thru hardware OS upgrades.</p>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="section container">
					<h2 class="aligncenter">Who We Are</h2>
					<ul class="bios">
						<li class="bio_container">
							<div class="bio_image andy"></div>
							<div class="bio">
								<h3>Andy J. Jacobs, <span>President</span></h3>
								<p>Andy has more than 25 years experience in the sales and implementation of barcode data collection software solutions worldwide. As President of Integrated Barcoding, Andy brings his vast personal knowledge of total data collection solutions to IB customers whether they are a local tool and die manufacturer or a global automotive industry provider. Andy has led the Integrated Barcoding's sales and technical teams to meet these customer’s needs by also offering hardware and media products to accompany their software solutions. Andy has worked closely with a number of IB software architects who have developed state-of-the art software solutions such as Integrated Barcoding’s flagship solution - QuikTrac®.</p>
							</div>
						</li>
						<li class="bio_container">
							<div class="bio_image ray"></div>
							<div class="bio">
								<h3>Ray Toren</h3>
								<p>Ray has over 30 years experience in the warehousing and manufacturing industries. As an end user of barcode data collection solutions, Ray has considerable expertise in a number of ERP solutions and systems. Since joining IB, Ray like Andy has worked closely with QuikTrac® software architects to continue to bring IB customers a total data collection solution to meet their needs. He has been crucial in a number of key IB software installations as well as by providing ongoing technical support to QuikTrac®  end users.</p>
							</div>
						</li>
					</ul>
				</div>			
			</div>
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>

