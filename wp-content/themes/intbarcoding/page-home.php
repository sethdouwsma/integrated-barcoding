<?php 
/* 
Template Name: Home Page
*/
?>

<?php get_header(); ?>


	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>

			<div class="content">
			
				<div class="section intro aligncenter">
					<h2>What We're All About</h2>
					<p>For the past 25 years, we have been providing solutions for integrating existing Enterprise Resource Planning (ERP) solutions seamlessly into the workers hands where the information is gathered and uploaded back into the host in real-time. Our QuikTrac software is used to help you do just that!</p>
				</div>
				
				<div class="section gray">
					<div class="container">
						<div class="quiktrac_hp">
							<img src="<?php echo get_template_directory_uri(); ?>/images/quiktrac_hp.png" alt="Quiktrac">
							<div class="quiktrac_overview">
								<h2>QuikTrac Overview</h2>
								<p>QuikTrac software is a set of tools that allows you to develop, deploy, and manage your mobile devices out in their environment. The QuikTrac® software is made up of 6 modules that provide you with everything you need to develop and maintain your own solution.</p>
								<a href="<?php echo get_permalink('15'); ?>">Learn More</a>
							</div>
						</div>
						
						<div class="one_solution">
							<h2 class="aligncenter">Everything you need in one solution</h2>
							<ul>
								<li>
									<p><em>No Host/RPG Programming</em></p>
									<p>No need to be a programmer! Just simply click, drag, and drop to create your screens on your project</p>
								</li>
								<li>
									<p><em>Keep Backend Code Untouched</em></p>
									<p>QuikTrac lies over the top of your existing code, therefore changes you make to the code do not affect it</p>
								</li>
								<li>
									<p><em>Constant Real-Time Connection</em></p>
									<p>When the host is available, you have real-time communication to the host and the user</p>
								</li>
								<li>
									<p><em>Offline Data Collection</em></p>
									<p>Even when the host is not available you can not only continue to collect data, but validate and store until host is available</p>
								</li>
								<li>
									<p><em>Design and Print Labels Easily</em></p>
									<p>Create and design your own label to print to any type of printer on your network</p>
								</li>
								<li>
									<p><em>Simultaneously Update All Devices</em></p>
									<p>Update changes to your projects are immediate and do not require touching the device</p>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="section aligncenter container">
					<div class="intro">
						<h2>Some of our clients</h2>
						<p>These are just a few of our clients that we have teamed up with with over the past 25 years to help provide solutions to their needs. Our software and hardware not only helped them reach their goals, but also provided a return on investment that ranged from 3 days to up to 24 months depending on the project.</p>
					</div>
					<p class="client_logos"><img src="<?php echo get_template_directory_uri(); ?>/images/client_logos.png" alt="Client Logos"></p>
					<div class="cs_coa">
						<p>See the results of our solutions being integrated.</p>
						<a href="<?php echo get_permalink('17'); ?>" class="button">View Case Studies</a>
					</div>
				</div>			
			</div>
				
		<?php endwhile; ?>
	<?php endif; ?>


<?php get_footer(); ?>
