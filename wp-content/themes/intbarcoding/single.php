<?php get_header(); ?>

	<div class="section content container">				
		<div class="inner">
			<?php while (have_posts()) : the_post(); ?>
				<article class="post_wrapper">
					<div class="article_header">
			        	<h2><?php the_title(); ?></h2>
			        	<p class="meta">Posted on <span><?php echo get_the_date(); ?></span> in <?php the_category(', ') ?></p>
			        </div>
			        <div class="article_body">
			        	<?php the_content(); ?>
			        </div>
				</article>
			<?php endwhile;?>
		</div><!-- end .inner -->
		
		<?php get_sidebar(); ?>
		
	</div><!-- end .content -->

<?php get_footer(); ?>